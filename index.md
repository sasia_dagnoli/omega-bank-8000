---
layout: default
title: Onboarding
---

## Welcome to Omega Bank!
This document is created to explain how to get set up with the code for Omega Bank

After cloning the repository to your local machine, please run the following command to build the docker image:

```bash
docker pull registry.gitlab.com/nannalea/omega-bank/djangoapp:latest
```

Then to start the docker container in development mode, run the following command:

```bash
RTE=dev docker-compose up
```
> You can now go to localhost:4000 to see the page up and running

If you wish to begin with a bit of data in the database, then you can access the shell in the docker container with this command:

```bash
RTE=dev docker-compose exec app sh
```

And inside the shell run the two following commands in order:
```bash
python manage.py provision
python manage.py demodata
```
> Now you should have a bit of data in your database for a start

Thank you!
