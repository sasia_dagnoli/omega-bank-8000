from django.core.management.base import BaseCommand
from bank_app.models import Rank


class Command(BaseCommand):
    def handle(self, **options):
        print('Provisioning...')
        if not Rank.objects.all():
            Rank.objects.create(name='Basic', value=1)
            Rank.objects.create(name='Silver', value=20)
            Rank.objects.create(name='Gold', value=30)
