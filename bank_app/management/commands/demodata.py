from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from bank_app.models import Account, CustomUser, Rank


class Command(BaseCommand):
    def handle(self, **options):
        print('Adding demo data....')
        bank = User.objects.create_user('bank4', email='bank@omegabank8000', password='Hejsa123')
        bank.first_name = 'Omega'
        bank.last_name = 'Bank'
        bank.username = 'bank4'
        bank.is_staff = True
        bank.save()
        bank_customUser = CustomUser(user=bank, phone='988823739')
        bank_account = Account.objects.create(user=bank, accountName='Bank Account 8000')
        amount = 4000000
        loan = Loan.objects.create(amount=amount, user=bank, accountName='Loan')
        loan.approved = True
        loan.save()
        Loan.approve_loan(loan)

        dummy_user = User.objects.create_user('dummy', email='dummy@dummy.com', password='mirror12')
        dummy_user.first_name = 'Dummy'
        dummy_user.last_name  = 'Dimwit'
        dummy_user.username = 'test'
        dummy_user.save()
        dummy_customer = CustomUser(user=dummy_user, phone='12345678')
        dummy_customer.save()
        dummy_account = Account.objects.create(user=dummy_user, accountName='opsparing')
        dummy_account.save()

