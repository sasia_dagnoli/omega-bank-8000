from .models import Rank, Loan, CustomUser, Account, TransactionId, Ledger
from django.http import HttpResponseRedirect
from django.shortcuts import render, reverse, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .forms import NewUserForm, NewCustomUserForm, TransferForm,TransferInternallyForm,  LoanForm, StaffNewAccountForm, NewAccountForm
from secrets import token_urlsafe
from django.db import IntegrityError
from django_otp.decorators import otp_required
from .errors import InsufficientFunds
import datetime
import random


@login_required
def index(request):
    if request.user.is_verified():
        if request.user.is_staff:
            return HttpResponseRedirect(reverse('bank_app:staff_portal'))
        else:
            return HttpResponseRedirect(reverse('bank_app:customer_portal'))
    else:
        return HttpResponseRedirect(reverse('two_factor:setup'))


@otp_required
def customerPortal(request):
    user = request.user.customuser
    accounts = request.user.customuser.accounts
    loans = request.user.customuser.loans

    context = {
        'user': user,
        'accounts': accounts,
        'loans': loans,
    }
    return render(request, 'bank_app/customer_portal.html', context)


def accountDetails(request, pk):
    user = request.user.customuser
    account = get_object_or_404(Account, pk=pk)
    movements = Ledger.objects.filter(account=pk)

    context = {
        'user': user,
        'account': account,
        'movements': movements,
    }
    return render(request, 'bank_app/account_details.html', context)

# @login_required
# def transfer()

# @login_required
# def staffPortal()

# @login_required


def createCustomer(request):
    assert request.user.is_staff, 'User needs to be staff to create a new user account'

    if request.method == 'POST':
        new_user_form = NewUserForm(request.POST)
        new_customuser_form = NewCustomUserForm(request.POST)
        if new_user_form.is_valid() and new_customuser_form.is_valid():
            username = new_user_form.cleaned_data['username']
            first_name = new_user_form.cleaned_data['first_name']
            last_name = new_user_form.cleaned_data['last_name']
            email = new_user_form.cleaned_data['email']
            password = new_user_form.cleaned_data['password']
            is_staff = new_user_form.cleaned_data['is_staff']
            phone = new_customuser_form.cleaned_data['phone']
            rank = new_customuser_form.cleaned_data['rank']
            is_superuser = is_staff
            try:
                user = User.objects.create_user(
                    username=username,
                    first_name=first_name,
                    last_name=last_name,
                    email=email,
                    password=password,
                    is_staff=is_staff,
                    date_joined=datetime.datetime.now(),
                    is_superuser=is_superuser,
                )
                CustomUser.objects.create(user=user, rank=rank, phone=phone, isActive=True)
                return staffCustomerView(request, user.pk)
            except IntegrityError:
                context = {
                    'new_user_form': NewUserForm(),
                    'new_customuser_form': NewCustomUserForm(),
                }
                return render(request, 'bank_app/create_customer.html', context)
        else:
            context = {
                'new_user_form': NewUserForm(request.POST),
                'new_customuser_form': NewCustomUserForm(request.POST),
            }
            return render(request, 'bank_app/create_customer.html', context)
    else:
        new_user_form = NewUserForm()
        new_customuser_form = NewCustomUserForm()
        context = {
            'new_user_form': new_user_form,
            'new_customuser_form': new_customuser_form
        }
        return render(request, 'bank_app/create_customer.html', context)


@login_required
def movementDetails(request, pk):
    movement = get_object_or_404(Ledger, pk=pk)
    context = {
        'movement': movement
    }
    return render(request, 'bank_app/movement_details.html', context)


@login_required
def createAccount(request):
    if request.user.is_staff:
        if request.method == 'POST':
            form = StaffNewAccountForm(request.POST)
            if form.is_valid():
                instance = form.instance
                Account.objects.create(
                    user=form.cleaned_data['user'],
                    createdBy=request.user,
                    createdAt=datetime.datetime.now(),
                    accountName=form.cleaned_data['accountName'],
                    id=str(random.randint(100000, 999999)),
                    isActive=True,
                    isLoan=False,
                )
                return HttpResponseRedirect(reverse('bank_app:staff_customer_view', args=(instance.user.pk,)))
        else:
            form = StaffNewAccountForm()
        context = {
            'form': form,
        }
        return render(request, 'bank_app/create_account.html', context)
    else:
        if not request.user.is_staff:
            count = request.user.customuser.count_self_created_accounts
            if not count < 5:
                context = {
                    'title': 'Account Creation Limit Reached',
                    'error': "You’ve reached the maximum number of self-created accounts."
                }
                return render(request, 'bank_app/create_account.html', context)
        if request.method == 'POST':
            form = NewAccountForm(request.POST)
            if form.is_valid():
                Account.objects.create(
                    user=request.user,
                    createdBy=request.user,
                    createdAt=datetime.datetime.now(),
                    accountName=form.cleaned_data['accountName'],
                    id=str(random.randint(100000, 999999)),
                    isActive=True,
                    isLoan=False,
                )
                return HttpResponseRedirect(reverse('bank_app:customer_portal'))
        else:
            form = NewAccountForm()
        context = {
            'form': form,
        }
        return render(request, 'bank_app/create_account.html', context)


def transaction_details(request, transactionId):
    movements = Ledger.objects.filter(transactionId=transactionId)

    accounts = request.user.customuser.accounts
    user = request.user.customuser
    total_balance = 0
    for a in accounts:
        balance = a.change_rank
        total_balance += balance
    if user.rank.name == 'Basic':
        print('BASIC')
        if total_balance >= 0:
            # rank = get_object_or_404(Rank)
            user.rank.value = 20
            user.save()

    loans = request.user.customuser.loans
    for lo in loans:
        if lo.balance == 0:
            lo.isActive = False
            lo.save()

    context = {
        'movements': movements,
    }

    return render(request, 'bank_app/transaction_details.html', context)


@login_required
def make_transfer(request):
    assert not request.user.is_staff, 'Staff user routing customer view'

    if request.method == 'POST':
        form = TransferForm(request.POST)
        form.fields['debit_account'].queryset = request.user.customuser.accounts
        form.fields['credit_account'].queryset = request.user.customuser.accounts_and_loans
        if form.is_valid():
            amount = form.cleaned_data['amount']
            debit_account = Account.objects.get(pk=form.cleaned_data['debit_account'].pk)
            debit_text = form.cleaned_data['debit_text']
            credit_account = Account.objects.get(pk=form.cleaned_data['credit_account'].pk)
            credit_text = form.cleaned_data['credit_text']
            try:
                transfer = Ledger.transfer(amount, debit_account, debit_text, credit_account, credit_text)
                return transaction_details(request, transfer)
            except InsufficientFunds:
                context = {
                    'title': 'Transfer Error',
                    'error': 'Insufficient funds for transfer'
                }
                return render(request, 'bank_app/error.html', context)
    else:
        form = TransferForm()
    form.fields['debit_account'].queryset = request.user.customuser.accounts
    form.fields['credit_account'].queryset = request.user.customuser.accounts_and_loans
    context = {
        'form': form,
    }
    return render(request, 'bank_app/make_transfer.html', context)


def internal_transfer(request):
    assert not request.user.is_staff, 'Staff user routing customer view'

    if request.method == 'POST':
        form = TransferInternallyForm(request.POST)
        form.fields['debit_account'].queryset = request.user.customuser.accounts
        if form.is_valid():
            amount = form.cleaned_data['amount']
            debit_account = Account.objects.get(pk=form.cleaned_data['debit_account'].pk)
            debit_text = form.cleaned_data['debit_text']
            credit_account = Account.objects.get(pk=form.cleaned_data['credit_account'])
            credit_text = form.cleaned_data['credit_text']
            try:
                transfer = Ledger.transfer(amount, debit_account, debit_text, credit_account, credit_text)
                return transaction_details(request, transfer)
            except InsufficientFunds:
                context = {
                    'title': 'Transfer error',
                    'error': 'Insufficient funds for transfer'
                }
                return render(request, 'bank_app/error.html', context)
    else:
        form = TransferInternallyForm()
    form.fields['debit_account'].queryset = request.user.customuser.accounts
    context = {
        'form': form
    }
    return render(request, 'bank_app/internal_transfer.html', context)


def bank_to_bank_transfer(request):
    return 'ban to bank transfer'


@login_required
def staffPortal(request):
    assert request.user.is_staff, 'Customer user routing staff view'
    user = request.user
    context = {
        'user': user
    }
    return render(request, 'bank_app/staff_portal.html', context)


@login_required
def staffSearch(request):
    assert request.user.is_staff, 'Customer user routing staff view'

    search = request.POST['search']
    customers = CustomUser.search(search)
    context = {
        'customers': customers,
    }
    return render(request, 'bank_app/staff_search.html', context)


# @login_required
# def createCustomer()

# @login_required
# def transfer()

# @login_required
# def staffPortal()

# @login_required
# def createCustomer()

@login_required
def take_loan(request):
    if not request.user.customuser.can_make_loan:
        context = {
            'title': 'Create loan error',
            'error': 'Loan could not be completed'
        }
        return render(request, 'bank_app/error.html', context)
    if request.method == 'POST':
        form = LoanForm(request.POST)
        if form.is_valid():
            amount = form.cleaned_data['amount']
            accountName = form.cleaned_data['loan_name']
            request.user.customuser.take_loan(accountName, amount)
            return HttpResponseRedirect(reverse('bank_app:customer_portal'))
    else:
        form = LoanForm()

    context = {
        'form': form,
    }
    return render(request, 'bank_app/take_loan.html', context)


def approve_loan(request):
    assert request.user.is_staff, 'You are not authorized to visit this page'
    loans = Loan.objects.all()
    if request.method == 'POST':
        # staff = request.user.customuser
        # user = CustomUser.objects.get(pk=Loan.user.pk)
        test = Loan()
        test.approve_loan()
        return HttpResponseRedirect(reverse('bank_app/staff_portal.html'))

    context = {
        'loans': loans
    }
    return render(request, 'bank_app/staff_approve_loans.html', context)


def approve(request, pk):
    loan = get_object_or_404(Loan, pk=pk)
    loan.approved = True
    loan.save()
    Loan.approve_loan(loan)
    return HttpResponseRedirect(reverse('bank_app:staff_approve_loans.html'))


def decline(request, pk):
    loan = get_object_or_404(Loan, pk=pk)
    loan.declined = True
    loan.save()
    return HttpResponseRedirect(reverse('bank_app:staff_approve_loans.html'))


def staffCustomerView(request, pk):
    assert request.user.is_staff, 'User needs to be staff to access this view'

    user = get_object_or_404(CustomUser, pk=pk)

    if request.method == "PATCH":
        account_id = list(request.PATCH.values())[0]
        if 'rank_value' in request.PATCH:
            rank = get_object_or_404(Rank, name=request.PATCH[f'{"rank_value"}'])
            user.rank = rank
        if 'active_value' in request.PATCH:
            user.isActive = request.PATCH[f'{"active_value"}']
        user.save()
        if 'remove_account_' + account_id in request.PATCH:
            account = get_object_or_404(Account, pk=account_id)
            account.isActive = "False"
            account.save()

    ranks = Rank.objects.all()
    accounts = user.accounts
    loans = user.loans
    context = {
        'ranks': ranks,
        'user': user,
        'accounts': accounts,
        'loans': loans,
    }
    return render(request, 'bank_app/staff_customer_view.html', context)
