from django.test import TestCase
from django.contrib.auth.models import User
from .models import Rank, CustomUser, Account
import datetime
from .forms import NewUserForm, NewCustomUserForm, LoanForm
from http import HTTPStatus
from django.test.client import Client


class UserTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        rank = Rank.objects.create(name='Basic', value=1)
        user = User.objects.create_user(
            username='test',
            first_name='test',
            last_name='user',
            email='test@user.com',
            password='testpassword',
            is_staff=False,
        )
        CustomUser.objects.create(
            user=user,
            rank=rank,
            phone='12345678',
            isActive=True
        )
        Account.objects.create(user=user, accountName='Test Account', isLoan=False)

    def test_first_name_max_length(self):
        user = User.objects.get(username='test')
        max_length = user._meta.get_field('first_name').max_length
        self.assertEqual(max_length, 150)

    def test_last_name_max_length(self):
        user = User.objects.get(username='test')
        max_length = user._meta.get_field('last_name').max_length
        self.assertEqual(max_length, 150)

    def test_full_name_returns_correct_format(self):
        customuser = CustomUser.objects.get(phone='12345678')
        expected_format = f'{customuser.user.first_name} {customuser.user.last_name}'
        self.assertEqual(customuser.full_name, expected_format)

    def test_user_cannot_take_loan(self):
        customuser = CustomUser.objects.get(phone='12345678')
        with self.assertRaises(AssertionError):
            customuser.take_loan('account name', 100.00)

    def test_account_name_is_string(self):
        user = User.objects.get(username='test')
        account = Account.objects.get(user=user)
        account_name = account.accountName
        self.assertIsInstance(account_name, str)

    def test_account_number_has_correct_length(self):
        user = User.objects.get(username='test')
        account = Account.objects.get(user=user)
        account_number = account.id
        length = 6
        self.assertEqual(length, len(str(account_number)))


class FormTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        rank = Rank.objects.create(name='Gold', value=30)
        staff = User.objects.create_user(
            username='staff',
            first_name='staff',
            last_name='staffuser',
            email='staff@test.com',
            password='staffpassword',
            is_staff=True
        )
        user = User.objects.create_user(
            username='testuser',
            first_name='testy',
            last_name='usery',
            email='testy@usery.com',
            password='password',
            is_staff=False
        )
        CustomUser.objects.create(
            user=staff,
            rank=rank,
            phone='12345679',
            isActive=True
        )
        CustomUser.objects.create(
            user=user,
            rank=rank,
            phone='123',
            isActive=True
        )

    def test_email_is_unique(self):
        form = NewUserForm(data={'email': 'staff@test.com'})

        self.assertEqual(form.errors['email'], ['User with entered email already exists'])

    def test_only_staff_can_get_page(self):
        self.client.login(username='testuser', password='password')

        with self.assertRaises(AssertionError):
            self.client.get('/create_customer/')

    def test_staff_can_visit_page(self):
        self.client.login(username='staff', password='staffpassword')
        response = self.client.get('/create_customer/')

        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_cannot_apply_for_negative_loan_amount(self):
        self.client.login(username='testuser', password='password')
        form = LoanForm(data={'amount': -10})

        self.assertEqual(form.errors['amount'], ['Amount must be positive.'])

    def test_user_can_go_to_loan_page(self):
        self.client.login(username='testuser', password='password')
        response = self.client.get('/take_loan/')

        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_user_can_apply_for_loan(self):
        self.client.login(username='testuser', password='password')
        response = self.client.post('/take_loan/', data={'loan_name': 'TestLoan', 'amount': 100})

        self.assertEqual(response.status_code, HTTPStatus.FOUND)
