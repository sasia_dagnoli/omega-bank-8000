from django import forms
from django.contrib.auth.models import User
from .models import CustomUser, Account
from django.core.exceptions import ObjectDoesNotExist


class NewAccountForm(forms.ModelForm):
    class Meta:
        model = Account
        widgets = {
            "user": forms.HiddenInput()
        }
        fields = ["accountName"]
        labels = {
            "accountName": "Name the account"
        }

    def clean(self):
        accountName = self.cleaned_data.get('accountName')
        # user = self.cleaned_data.get('accountName').pk
        if Account.objects.filter(accountName=accountName):
            self._errors['accountName'] = self.error_class(['An account with that name already exists.'])
        return self.cleaned_data


class StaffNewAccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ["user", "accountName"]
        labels = {
            "user": "Choose the Customer you are creating an account for",
            "accountName": "Name the account"
        }

    def clean(self):
        customer = self.cleaned_data.get('user')
        accountName = self.cleaned_data.get('accountName')
        if Account.objects.filter(user=customer, accountName=accountName):
            self._errors['accountName'] = self.error_class(['Customer already has an account with that name.'])
        return self.cleaned_data


class NewUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password', 'is_staff')

    # removes help_text from all fields
    def __init__(self, *args, **kwargs):
        super(NewUserForm, self).__init__(*args, **kwargs)
        for fieldname in self.fields:
            self.fields[fieldname].help_text = None

    # check user with entered email or username does not already exists
    def clean(self):
        super().clean()
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if User.objects.filter(email=email):
            self._errors['email'] = self.error_class(['User with entered email already exists'])
        if User.objects.filter(username=username):
            self._errors['username'] = self.error_class(['Username is already in use. Try another one'])
        return self.cleaned_data


class NewCustomUserForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ('phone', 'rank')

    def clean(self):
        return self.cleaned_data


class TransferForm(forms.Form):
    amount = forms.DecimalField(label='Amount', max_digits=10)
    debit_account = forms.ModelChoiceField(label='Debit Account', queryset=CustomUser.objects.none())
    debit_text = forms.CharField(label='Debit account text', max_length=25)
    # credit_account = forms.IntegerField(label='Credit account number')
    credit_account = forms.ModelChoiceField(label='Credit Account', queryset=CustomUser.objects.none())
    credit_text = forms.CharField(label='Credit account text', max_length=25)

    def clean(self):
        super().clean()

        if self.cleaned_data.get('amount') < 0:
            self._errors['amount'] = self.error_class(['Amount must be positive.'])

        return self.cleaned_data


class TransferInternallyForm(forms.Form):
    amount = forms.DecimalField(label='Amount', max_digits=10)
    debit_account = forms.ModelChoiceField(label='Debit Account', queryset=CustomUser.objects.none())
    debit_text = forms.CharField(label='Debit account text', max_length=25)
    credit_account = forms.IntegerField(label='Credit account number')
    credit_text = forms.CharField(label='Credit account text', max_length=25)

    def clean(self):
        super().clean()

        credit_account = self.cleaned_data.get('credit_account')
        try:
            Account.objects.get(pk=credit_account)
        except ObjectDoesNotExist:
            self._errors['credit_account'] = self.error_class(['Credit account does not exist'])

        if self.cleaned_data.get('amount') < 0:
            self._errors['amount'] = self.error_class(['Amount must be positive'])

        return self.cleaned_data


class LoanForm(forms.Form):
    loan_name = forms.CharField(label='Name for loan', max_length=25)
    amount = forms.DecimalField(label='Amount', max_digits=10)

    def clean(self):
        super().clean()

        # loan_name = self.cleaned_data.get('loan_name')

        if self.cleaned_data.get('amount') < 0:
            self._errors['amount'] = self.error_class(['Amount must be positive.'])
