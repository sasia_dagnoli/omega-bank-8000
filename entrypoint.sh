#!/bin/sh

echo "** RTE mode: $RTE"

    python manage.py check

case "$RTE" in
    dev )
        echo "** Development Mode"
        python manage.py makemigrations
        python manage.py migrate
        python manage.py runserver 0.0.0.0:4000
        ;;
    test )
        echo "** Test Mode"
        coverage run --source="." --omit=manage.py manage.py test --verbosity 2
        coverage report -m --fail-under=50
        ;;
    prod )
        echo "** Prod Mode"
        # python manage.py check -- deploy
        ;;
esac
